#!/usr/bin/env python3

import configparser
import json
import datetime
import sys
from woocommerce import API
import pprint
import logging
import logging.handlers
import os

# Get some variables outside this script
config = configparser.ConfigParser()

try:
    f = open("config.cfg", 'rb')
except OSError:
    print("Could not open/read file: config.cfg")
    sys.exit()

with f:
    config.read("config.cfg")
    domain = config['DETAILS']['DOMAIN']
    key = config['DETAILS']['KEY']
    secret = config['DETAILS']['SECRET']
    Orders_Per_Page = config['DETAILS']['ORDERSPERPAGE']

print("Connecting to " + domain + " with " + key + " and secret " + secret)

wcapi = API(
    url="https://" + domain,
    consumer_key=key,
    consumer_secret=secret,
    wp_api=True,
    version="wc/v3"
)

class bcolors:
    HEADER = '\033[95m'
    OKBLUE = '\033[94m'
    OKCYAN = '\033[96m'
    OKGREEN = '\033[92m'
    WARNING = '\033[93m'
    FAIL = '\033[91m'
    ENDC = '\033[0m'
    BOLD = '\033[1m'
    UNDERLINE = '\033[4m'

now = datetime.date.today()
#now = datetime.datetime.strptime('2021/01/01','%Y/%m/%d').date()
print(bcolors.HEADER + "Now: " + str(now) + bcolors.ENDC)
# 2021-07-07

print("**********************************************")

params = {
    'status': 'processing',
    'per_page': Orders_Per_Page,
    'page': 1
}

logging.basicConfig(format='%(asctime)s %(levelname)-8s [%(filename)s:%(lineno)d] %(message)s', filename='debug.log', filemode='a', level=logging.INFO, datefmt='%d-%m-%Y:%H:%M:%S',)
logger = logging.getLogger('my_app')

lOrders = wcapi.get("orders", params=params).json()
#print(lOrders)
for dOrder in lOrders:
    #print(dOrder)
    order_id = dOrder.get('id')
    print(order_id)
    status = dOrder.get('status')
    sDelivery_date = dOrder.get('meta_data', {})[1].get('value')
    #print(sDelivery_date)
    sDelivery_time = dOrder.get('meta_data', {})[4].get('value')
    sFormatted_Delivery_date = datetime.datetime.strptime(sDelivery_date,'%Y/%m/%d').date()

    print("Order " + str(order_id) + " with status " + bcolors.BOLD + status + bcolors.ENDC + " is to be picked up on " + str(sFormatted_Delivery_date) + " at " + sDelivery_time)
    logger.info("Order " + str(order_id) + " with status " + bcolors.BOLD + status + bcolors.ENDC + " is to be picked up on " + str(sFormatted_Delivery_date) + " at " + sDelivery_time)

    if sFormatted_Delivery_date > now:
        print(bcolors.OKBLUE + "Order " + str(order_id) + " NOT to be finished, date " + str(sFormatted_Delivery_date) + " is in the future compared to " + str(now) + bcolors.ENDC)
        logger.info(bcolors.OKBLUE + "Order " + str(order_id) + " NOT to be finished, date " + str(sFormatted_Delivery_date) + " is in the future compared to " + str(now) + bcolors.ENDC)
    else:
        print(bcolors.OKGREEN + "Order " + str(order_id) + " to be finished, date " + str(sFormatted_Delivery_date) + " is in the past compared to " + str(now) + bcolors.ENDC)
        logger.warning(bcolors.OKGREEN + "Order " + str(order_id) + " to be finished, date " + str(sFormatted_Delivery_date) + " is in the past compared to " + str(now) + bcolors.ENDC)

        # Complete order
        data = {
            "status": "completed"
        }
        dResponse_Completed_Order = wcapi.put("orders/" + str(order_id), data).json()

        # Print response
        sResponse_Completed_Order = dResponse_Completed_Order.get('status', {})
        if sResponse_Completed_Order == "completed":
            print(bcolors.OKGREEN + "Order " + str(order_id) + ": " + sResponse_Completed_Order + bcolors.ENDC)
            logger.info(bcolors.OKGREEN + "Order " + str(order_id) + ": " + sResponse_Completed_Order + bcolors.ENDC)
        else:
            print(bcolors.FAIL + "Order " + str(order_id) + ": NOT " + sResponse_Completed_Order + bcolors.ENDC)
            logger.warning(bcolors.FAIL + "Order " + str(order_id) + ": NOT " + sResponse_Completed_Order + bcolors.ENDC)



    print("-------------------------------------------------------------")
