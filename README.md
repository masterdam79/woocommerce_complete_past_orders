# Woocommerce Complete Past Orders

This little script can be run every day at a certain time, triggered by a cron or whatnot, to query Woocommerce Orders API, get all orders that are in progress and complete those that are past tense relative to the current datetime.

This can be useful if you run a webshop where customers can specify an order to be picked up in a physical shop and paid therein.
Therefor orders linger until closed manually.

# Setup

1. Install ```woocommerce``` using ```pip```
  - ``python3 -m pip install woocommerce```
2. Rename ```config``` to ```config.cfg```
  - Adapt values in ```config.cfg``` to match your environment
